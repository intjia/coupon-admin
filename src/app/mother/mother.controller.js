(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('MotherController', MotherController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function MotherController($timeout, $window, $log, moment, RestSer, AlertSer, Upload) {
    var vm = this;
    vm.alerts = [];
    vm.moment = moment;
    vm.getTime = function(date){
      return (date ? new Date(date) : new Date()).getTime();
    };
    vm.now = vm.getTime();

    vm.fields = {
      name: {
        key: '姓名',
        type: 'text'
      },
      tel: {
        key: '电话',
        type: 'text'
      },
      addr: {
        key: '家庭住址',
        type: 'text'
      },
      babyName: {
        key: '宝宝姓名',
        type: 'text'
      },
      birthday: {
        key: '生日',
        type: 'text'
      },
      group: {
        key: '所在群',
        type: 'select'
      },
      week:{
        key: '怀孕几周',
        type: 'select'
      }
    };

    // 是否已选中
    vm.isSelected = function(name){
      return !!(vm.add && vm.add.attr && vm.add.attr[name]);
    };

    // upCheck
    vm.upCheck = function($event, name){
      var checkbox = $event.target;
          checkbox = checkbox.checked ?  true : false;

          if(!vm.add.attr){
            vm.add.attr = {};
          }
          // 选中
          if(checkbox){
            vm.add.attr[name] = vm.fields[name];
          }else{
            delete vm.add.attr[name];
          }

          // console.log(vm.add.attr);
          // console.log('checkbox', checkbox);

    };

    vm.getList = function(page, limit) {
      vm.result = false;
      if (vm.editor) {
        vm.editor = false;
      }

      if(page && page === vm.page){
        return;
      }

      page = parseInt(page) || 1;
      limit = limit || 30;
      vm.mothers = [];
      // 取妈妈Pa列表数据
      RestSer.mother.find({limit: limit, skip: (page - 1)* limit, descending: 'endTime'}).then(function(mothers) {
        vm.mothers = mothers;
      }, function() {

      });

      /**
       * 查询记录总条目数
       * @return {[type]} [description]
       */
      RestSer.mother.count().then(function(count){
        var len = Math.ceil(count/limit), i, pages = [];
        for( i = 1; i < len + 1; i++){
          pages.push(i);
        }
        vm.pages = pages;
        vm.page = page;
      });
    };

    vm.getList();

    // 显示添加视图
    vm.addTap = function(item) {
      vm.editor = true;
      if(item){
        item = angular.copy(item);
        if(item.startTime){
          item.startTime = moment(item.startTime.iso).format('YYYY-MM-DD HH:mm');
        }
        if(item.endTime){
          item.endTime = moment(item.endTime.iso).format('YYYY-MM-DD HH:mm');
        }
        if(item.regTime){
          item.regTime = moment(item.regTime.iso).format('YYYY-MM-DD HH:mm');
        }

      }
      vm.add = item || {
        sort: 0
      };
    };


    /**
     * 创建，修改
     * 如果包含 objectId 即为修改,不含有 ID 即为创建
     */
    vm.save = function() {
      var add = angular.copy(vm.add),
        id = add.objectId,
        text = '添加',
        action;

      add._date = {
        startTime: new Date(add.startTime),
        endTime: new Date(add.endTime),
        regTime: new Date(add.regTime)
      };
      delete add.objectId;
      delete add.startTime;
      delete add.endTime;
      delete add.regTime;


      if (id) {
        action = RestSer.mother.put(id, add);
        text = '修改';
      } else {
        action = RestSer.mother.add(add);
      }

      vm.loading = true;
      action.then(function() {
        AlertSer.show({
          type: 'success',
          msg: text + '妈妈Pa成功'
        });
        vm.loading = false;
        vm.addTap();
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: text + '妈妈Pa失败'
        });
        vm.loading = false;
      });
    };

    /**
     * 删除单条记录
     */
    vm.remove = function() {
      vm.loading = true;
      // var remove = $window.confirm('确定删除「'+ item.name +'」妈妈趴?');
      // if(!remove){
      //   return false;
      // }
      RestSer.mother.remove(vm.add.objectId).then(function() {
        AlertSer.show({
          type: 'success',
          msg: '删除妈妈Pa活动成功'
        });
        vm.getList();
        vm.loading = false;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: '添加妈妈Pa活动失败'
        });
        vm.loading = false;
      });
    };


    // upload on file select or drop
    vm.upload = function(file, field) {
      Upload.upload({
        url: '/api/upload',
        data: {
          file: file
        }
      }).then(function(resp) {
        $log.log(resp);
        var data = resp.data;
        vm.add[field] = data.url;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: '上传图片失败'
        });
      }, function() {

      });
    };

    // 查看结果
    vm.showResult = function(id){
      vm.result = id;
    };
  }
})();
