(function() {
  'use strict';

  angular
    .module('webProject')
    .service('RestSer', RestSer);

  /** @ngInject */
  function RestSer(Restangular, host) {
    function Ser(url) {
      this.host = host + url + '/';
    }

    Ser.prototype.count = function(query) {
      return Restangular.one(this.host, 'count').get(query);
    };

    Ser.prototype.get = function(id) {
      return Restangular.one(this.host, id).get();
    };

    Ser.prototype.find = function(query) {
      return Restangular.all(this.host).getList(query);
    };

    Ser.prototype.remove = function(id) {
      return Restangular.one(this.host, id).remove();
    };
    Ser.prototype.add = function(post) {
      return Restangular.all(this.host).post(post);
    };
    Ser.prototype.put = function(id, post) {
      return Restangular.all(this.host + id).post(post);
    };



    var Class = {};
    Class.feature = new Ser('Feature');
    Class.mother = new Ser('MotherPa');
    Class.mpsignin = new Ser('MPSignIn');
    Class.signin = new Ser('SignIn');
    Class.votes = new Ser('Votes');
    Class.lottery = new Ser('Lottery');
    Class.lotterylog = new Ser('LotteryLog');
    Class.gift = new Ser('Gift');
    Class.user = new Ser('User');
    Class.coupon = new Ser('Coupon');

    Class.role = {};
    Class.role.find = function(roleName, query){
      return Restangular.all(host + 'role/' + roleName).getList(query || {});
    };
    Class.role.count = function(roleName, query){
      return Restangular.one(host + 'role/' + roleName, 'count').get(query || {});
    };
    Class.role.add = function(roleName, post){
      return Restangular.all(host + 'role/' + roleName).post(post);
    };
    Class.role.put = function(id, roleName, post){
      return Restangular.all(host + 'role/' + roleName +'/'+ id).post(post);
    };
    Class.role.remove = function(id, roleName){
      return Restangular.one(host + 'role/' + roleName, id).remove();
    };

    return Class;
  }

})();
