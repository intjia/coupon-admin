/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('webProject')
    .constant('malarkey', malarkey)
    .constant('host', '/api/')
    .constant('FirebaseUrl', 'https://yejingji.firebaseio.com/midNight/')
    .constant('FirebaseUser', 'https://yejingji.firebaseio.com/users/')
    .constant('moment', moment);
})();
