(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('ExchangeController', ExchangeController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function ExchangeController($firebaseRef,$filter, excelSer, $scope, $firebaseArray, $timeout, $window, $log, moment, RestSer, AlertSer, Upload, authSer) {
    var vm = this;
    vm.moment = moment;
    vm.activitys = $firebaseArray($firebaseRef.activity);
    vm.activityObj = {};
    vm.businessObj = {};
    vm.loading = true;
    vm.business =  $firebaseArray($firebaseRef.userBusiness);


    var list = $firebaseArray($firebaseRef.prizes.orderByChild('exchanged'));
      list.$loaded()
        .then(function(x) {
          console.log('加载结果完成');
          forMat();
        })
        .catch(function(error) {
          AlertSer.show({
            type:'danger',
            msg:'加载结果出错请重试'
          });
          console.log('Error:', error);
        });
    // vm.prizes = $firebaseArray($firebaseRef.prizes);

    /**
     * 格式化结果数据
     * 遍历所有数据
     * 取出各个数据的活动名称，商家名称，父级商家，红包名称，红包金额
     */

     function forMat(){
       var act = vm.activitysObj,
           bus = vm.businessObj,
           i, len,
           data = list;

           console.log('list', list.length);

       if(
          list && (list.length || list.length !== 0) &&
          vm.activitys && (vm.activitys.length || vm.activitys.length === 0) &&
          vm.business && (vm.business.length || vm.business.length === 0)
        ){
              var arr = [];
          for(i = 0, len = data.length; i < len; i++){
              if(data[i].exchanged){

              // 活动名称
              if(act.hasOwnProperty(data[i].activity)){
                data[i].activityName = act[data[i].activity].name;

                if(act[data[i].activity].hasOwnProperty(data[i].prizeKey)){
                  data[i].prizeName = act[data[i].activity][data[i].prizeKey].name;
                  data[i].price = act[data[i].activity][data[i].prizeKey].price;
                }

              }

              // 商家名称
              if(bus.hasOwnProperty(data[i].exchanged)){
                data[i].business = bus[data[i].exchanged].name;
              }

              // 商家名称
              if(data[i].exchanged !== data[i].parent && bus.hasOwnProperty(data[i].parent)){
                data[i].businessParent = bus[data[i].parent].name;
              }

              arr.push(data[i]);
            }

          }

          arr.sort(function(a, b){
            return a.exchangedDate < b.exchangedDate;
          });

          vm.cachePrizes = arr;
          vm.showPrizes = angular.copy(arr);
          //  计算统计结果
           vm.tongji();
          vm.loading = false;
        }

     }

     /**
      * 筛选结果
      * 通过 活动名称，母商家，子商家。
      * @return {[type]} [description]
      */
     vm.searchPrizes = function(){
       var activity = $scope.activity;
       var business = $scope.business;
       var business_child =  $scope.business_child;
       var result = {price: 0, length: 0};
       vm.loading = true;
       if(activity && business){

         if(business_child){
           vm.showPrizes = $filter('filter')(vm.cachePrizes, {'exchanged':business_child, 'activity': activity, 'parent': business});
         }else{
           console.log('guolv');
           vm.showPrizes = $filter('filter')(vm.cachePrizes, {'activity': activity, 'parent': business});
         }

         vm.showPrizes = $filter('orderBy')(vm.showPrizes, 'exchangedDate');
       }
      //  计算统计结果
       vm.tongji();
       vm.loading = false;

     };

     /**
      * 统计结果显示
      * 遍历所有的显示结果，
      * 计算出总红包个数，总红包金额。
      * 以及各个商家的总红包个数，总红包金额
      * @return {[type]} [description]
      */
     vm.tongji = function(){
       var result = {all:{length: 0, price: 0, list:{}}};
        angular.forEach(vm.showPrizes, function(data){

          result.all.length++;

          // 商户统计
          if(data.business){

            if(result.hasOwnProperty(data.business)){
              result[data.business].length++;
            }else{
              result[data.business] = {length: 1, price: 0, list:{}};
            }

          }

          // 全部汇总
          if(angular.isNumber(data.price)){
            result.all.price += data.price;
            if(result.all.list.hasOwnProperty(data.price)){
              result.all.list[data.price]++;
            }else{
              result.all.list[data.price] = 1;
            }

            // 子商家总金额，及各个红包个数
            if(result[data.business]){
              result[data.business].price = result[data.business].price ? result[data.business].price + data.price : data.price;
              result[data.business].list[data.price] = result[data.business].list[data.price] ? result[data.business].list[data.price] + 1 : 1;
            }

          }
        });

        vm.showTongji = result;
        // hui
        console.log('汇总结果', result);

     };

     /**
      * 下载当前显示的结果
      * 通过活动名称，母商家名称，商家名称。拼接导出 Excel 文件名称。文件尾部加入导出的时间戳
      * 拼接完名称后，
      * 首先记录统计数据。（全部兑换数据汇总，各个商家兑换信息汇总）
      * 然后写入兑换详情
      * @return {[type]} [description]
      */
     vm.download = function(){
       var arr = [],
          outName = '',
            actName = vm.activitysObj[$scope.activity],
            business = vm.businessObj[$scope.business],
            business_child = vm.businessObj[$scope.business_child];

       if(actName && actName.name){
         outName += actName.name;
       }

       if(business && business.name){
         outName += '--' + business.name;
       }

       if(business_child && business_child.name){
         outName += '--' + business_child.name;
       }

       outName += '兑换详情,截止日期';

       if(vm.showTongji && vm.showTongji.all){
         arr.push(['商户名称',	'总金额',	'红包个数',	'兑换详细']);
         angular.forEach(vm.showTongji, function(data, key){
           key = key === 'all' ? '全部商家' : key;
           var arrOne = [key, data.price, data.length];
           var result = '';
          //  遍历结果详细
           angular.forEach(data.list, function(length, name){
              result += name + '元：' + length + '个  ,   ';
           });
           arrOne.push(result);
           arr.push(arrOne);
         });
       }


       if(vm.showPrizes && vm.showPrizes.length){
        //  标题栏
         arr.push(['商户名称', '红包金额', '兑换时间',	'红包名称',	'父级商家',	 '活动名称'	]);
        //  结果
         angular.forEach(vm.showPrizes, function(data){
           arr.push([data.business || '', data.price || '', moment(data.exchangedDate).format('YYYY-MM-DD HH:mm') || '', data.prizeName || '', data.businessParent || '', data.activityName || '']);
         });
         excelSer.outPut(arr, outName + moment().format('YYYY-MM-DD HH:mm'));
       }

     };

    /**
     * 加载所有奖品列表
     * @return {[type]} [description]
     */
    // vm.prizes.$loaded()
    //     .then(function(data){
    //       console.log('prizes', data);
    //     })
    //     .catch(function(err){
    //       console.error('加载奖品结果失败', err);
    //     });

    /**
     * 监听数据变化
     * 初始化后默认显示第一条数据
     * 数据变化后重新渲染结果
     */
    vm.business.$loaded()
      .then(function(x) {
        var obj = {};
       //  如果没有默认选中活动，则默认选中第一条活动
          if(vm.business.length){
            for(var i = 0, len = vm.business.length; i < len; i++){
              obj[vm.business[i].$id] = vm.business[i];
            }
          }
          vm.businessObj = obj;
          forMat();
          console.log('businessdfsds');
      })
      .catch(function(error) {
        AlertSer.show({
          type:'danger',
          msg:'加载商家列表出错请重试'
        });
        console.log('Error:', error);
      });
    // $load(function(event) {
    //
    // });

    vm.activitys.$loaded()
        .then(function(){
          var obj = {};
         //  如果没有默认选中活动，则默认选中第一条活动
          if(!$scope.activity){

            if(vm.activitys.length){
              $scope.activity = vm.activitys[0].$id;
            }

          }
          if(vm.activitys.length){
            for(var i = 0, len = vm.activitys.length; i < len; i++){
              if(angular.isObject(vm.activitys[i].prize)){
                for(var x in vm.activitys[i].prize){
                  vm.activitys[i][x] = vm.activitys[i].prize[x];
                }
              }
              obj[vm.activitys[i].$id] = vm.activitys[i];
            }
          }
          vm.activitysObj = obj;
          forMat();

        })
        .catch(function(error) {
          AlertSer.show({
            type:'danger',
            msg:'加载活动列表出错请重试'
          });
          console.log('Error:', error);
        });

    // vm.activitys.$watch(function(event) {
    //   console.log('obj', obj);
    // });

    // $firebaseRef.prizes.once('value', function(data){
    //     console.log('value', data.val());
    // });

    // 监听选择商家,查询结果
    // vm.search = function(){
    //
    //   var activity = $scope.activity;
    //   var business = $scope.business;
    //   var business_child =  $scope.business_child;
    //   // console.log('business', business);
    //   // console.log('activity', activity);
    //   // console.log('business_child', business_child);
    //   vm.loading = true;
    //
    //   // if(business_child){
    //   //
    //   // }
    //
    //   // $firebaseRef.prizes.orderByChild('exchanged').equalTo(business).once('value', function(data){
    //   //   vm.prizesList = data.val();
    //   //   console.log('vm.prizesList', vm.prizesList);
    //   //   // 格式化结果
    //   //   vm.showData();
    //   //   vm.loading = false;
    //   //   // //
    //   //   // $scope.$apply(function(){
    //   //   // });
    //   // });
    //
    // };
    //
    // vm.showData = function(){
    //   console.log('vm.prizesList', vm.prizesList);
    //   var act = vm.activitysObj,
    //       bus = vm.businessObj,
    //       data = vm.prizesList;
    //       var arr = [];
    //   for(var i in data){
    //     // 活动名称
    //     if(act.hasOwnProperty(data[i].activity)){
    //       data[i].activityName = act[data[i].activity].name;
    //
    //       if(act[data[i].activity].hasOwnProperty(data[i].prizeKey)){
    //         data[i].prizeName = act[data[i].activity][data[i].prizeKey].name;
    //         data[i].price = act[data[i].activity][data[i].prizeKey].price;
    //       }
    //
    //     }
    //
    //     // 商家名称
    //     if(bus.hasOwnProperty(data[i].exchanged)){
    //       data[i].business = bus[data[i].exchanged].name;
    //     }
    //
    //     // 商家名称
    //     if(data[i].exchanged !== data[i].parent && bus.hasOwnProperty(data[i].parent)){
    //       data[i].businessParent = bus[data[i].parent].name;
    //     }
    //
    //     arr.push(data[i]);
    //
    //   }
    //   $scope.$apply(function(){
    //     arr.sort(function(a, b){
    //       return a.exchangedDate < b.exchangedDate;
    //     });
    //     vm.prizesList = arr;
    //     vm.loading = false;
    //   });
    //   console.log('data', data  );
    //
    // };
    //

    // console.log('进来了')
    // vm.prizes.$watch(function(){
    //
    //   console.log('vm.prizes', vm.prizes);
    //
    // });



  }
})();
