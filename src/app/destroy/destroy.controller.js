(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('DestroyController', DestroyController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function DestroyController($timeout, $window, $log, moment, RestSer, AlertSer, Upload) {
    var vm = this;
    vm.alerts = [];
    vm.moment = moment;
    vm.isActive = true;


    vm.getTime = function(date){
      return (date ? new Date(date) : new Date()).getTime();
    };
    vm.now = vm.getTime();


    // 加载商户列表
    vm.getUsers = function(){
      RestSer.role.find('business').then(function(users) {
        vm.users = users;
        vm.loadingData = false;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg:'商家加载失败,请重试'
        });
      });
    };
    vm.getUsers();

    // 是否已选中
    vm.isSelected = function(name){
      return !!(vm.add && vm.add.attr && vm.add.attr[name]);
    };

    // upCheck
    vm.upCheck = function($event, name){
      var checkbox = $event.target;
          checkbox = checkbox.checked ?  true : false;

          if(!vm.add.attr){
            vm.add.attr = {};
          }
          // 选中
          if(checkbox){
            vm.add.attr[name] = vm.fields[name];
          }else{
            delete vm.add.attr[name];
          }

          // console.log(vm.add.attr);
          // console.log('checkbox', checkbox);

    };

    vm.getList = function(page, limit) {
      // 此行为侧边导航状态变更代码
      vm.isActive = true;
      vm.result = false;
      if (vm.editor) {
        vm.editor = false;
      }

      if(page && page === vm.page){
        return;
      }

      page = parseInt(page) || 1;
      limit = limit || 30;
      vm.gifts = [];
      // 取商品列表数据
      RestSer.gift.find({limit: limit, skip: (page - 1)* limit, descending: 'end', 'include[0]':'user'}).then(function(gifts) {
        vm.gifts = gifts;
      }, function() {

      });

      /**
       * 查询记录总条目数
       * @return {[type]} [description]
       */
      RestSer.gift.count().then(function(count){
        var len = Math.ceil(count/limit), i, pages = [];
        for( i = 1; i < len + 1; i++){
          pages.push(i);
        }
        vm.pages = pages;
        vm.page = page;
      });
    };

    vm.getList();

    // 显示添加视图
    vm.addTap = function(item) {
      // 此行为侧边导航选中状态变更用
      vm.isActive = false;
      vm.editor = true;
      if(item){
        item = angular.copy(item);
        if(item.start){
          item.start = moment(item.start.iso).format('YYYY-MM-DD HH:mm');
        }
        if(item.end){
          item.end = moment(item.end.iso).format('YYYY-MM-DD HH:mm');
        }

        if(item.user){
          item.user = item.user.objectId;
        }

      }
      vm.add = item || {
        sort: 0
      };
    };


    /**
     * 创建，修改
     * 如果包含 objectId 即为修改,不含有 ID 即为创建
     */
    vm.save = function() {
      var add = angular.copy(vm.add),
        id = add.objectId,
        text = '添加',
        action;

      add._date = {
        start: new Date(add.start),
        end: new Date(add.end)
      };
      delete add.objectId;
      delete add.start;
      delete add.end;
      delete add.updatedAt;
      delete add.createAt;


      if (id) {
        action = RestSer.gift.put(id, add);
        text = '修改';
      } else {
        action = RestSer.gift.add(add);
      }

      vm.loading = true;
      action.then(function() {
        AlertSer.show({
          type: 'success',
          msg: text + '商品成功'
        });
        vm.loading = false;
        vm.addTap();
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: text + '商品失败'
        });
        vm.loading = false;
      });
    };

    /**
     * 删除单条记录
     */
    vm.remove = function() {
      vm.loading = true;
      // var remove = $window.confirm('确定删除「'+ item.name +'」妈妈趴?');
      // if(!remove){
      //   return false;
      // }
      RestSer.gift.remove(vm.add.objectId).then(function() {
        AlertSer.show({
          type: 'success',
          msg: '删除商品活动成功'
        });
        vm.getList();
        vm.loading = false;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: '添加商品活动失败'
        });
        vm.loading = false;
      });
    };


    // upload on file select or drop
    vm.upload = function(file, field) {
      Upload.upload({
        url: '/api/upload',
        data: {
          file: file
        }
      }).then(function(resp) {
        $log.log(resp);
        var data = resp.data;
        vm.add[field] = data.url;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: '上传图片失败'
        });
      }, function() {

      });
    };

    // 查看结果
    vm.showResult = function(id){
      vm.result = id;
    };
  }
})();
