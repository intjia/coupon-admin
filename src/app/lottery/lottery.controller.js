(function() {
  'use strict';

  angular
    .module('webProject')
    .filter('prizeLength', function(){
        return function(input){
          var len = 0;
          if(angular.isArray(input)){
            angular.forEach(input, function(item){
              if(item && angular.isNumber(item.number)){
                len += item.number;
              }
            });
          }
          return len;
        };
    })
    .controller('LotteryController', LotteryController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function LotteryController($timeout, $log, moment, RestSer, AlertSer, Upload) {
    var vm = this;
    vm.alerts = [];
    vm.moment = moment;
    vm.getTime = function(date){
      return (date ? new Date(date) : new Date()).getTime();
    };
    vm.now = vm.getTime();
    // 奖品类型
    vm.prize = [{
      key: 'firstPrize',
      val: '一等奖'
    },{
      key: 'secondPrize',
      val: '二等奖'

    },{
      key: 'thirdPrize',
      val: '三等奖'

    },{
      key: 'otherPrize',
      val: '参与奖'

    }];
    // 抽奖类型
    vm.types = [
      {
        name: '大转盘',
        key: 'zhuan'
      },
      {
        name: '刮刮卡',
        key: 'gua'
      },
      {
        name: '老虎机',
        key: 'tiger'
      }
    ]; //抽奖类型

    vm.getList = function(page, limit) {
      if (vm.editor) {
        vm.editor = false;
      }

      if(page && page === vm.page){
        return;
      }

      page = parseInt(page) || 1;
      limit = limit || 30;
      vm.lotterys = [];
      // 取抽奖列表数据
      RestSer.lottery.find({limit: limit, skip: (page - 1)* limit}).then(function(lotterys) {
        vm.lotterys = lotterys;
      }, function() {

      });

      /**
       * 查询记录总条目数
       * @return {[type]} [description]
       */
      RestSer.lottery.count().then(function(count){
        var len = Math.ceil(count/limit), i, pages = [];
        for( i = 1; i < len + 1; i++){
          pages.push(i);
        }
        vm.pages = pages;
        vm.page = page;
      });
    };

    vm.getList();

    // 显示添加视图
    vm.addTap = function(item) {
      vm.editor = true;
      if(item){
        item = angular.copy(item);
        if(item.startTime){
          item.startTime = moment(item.startTime.iso).format('YYYY-MM-DD HH:mm');
        }
        if(item.endTime){
          item.endTime = moment(item.endTime.iso).format('YYYY-MM-DD HH:mm');
        }
      }
      vm.add = item || {

      };
    };

    /**
     * 奖项添加奖品
     * @return {[type]} [description]
     */
    vm.addPrize = function(add, key){
      if(!angular.isObject(add[key])){
        add[key] = {};
      }
      if(add[key].prize && add[key].prize.length){
          add[key].prize.push({});
      }else{
        add[key].prize = [{}];
      }

    };

    /**
     * 删除奖品项
     */
    vm.removePrize = function(item, index){
      if(angular.isArray(item.prize) && angular.isNumber(index)){
        item.prize.splice(index, 1);
      }
    };


    /**
     * 创建，修改
     * 如果包含 objectId 即为修改,不含有 ID 即为创建
     */
    vm.save = function() {
      var add = angular.copy(vm.add),
        id = add.objectId,
        text = '添加',
        action;

      add._date = {
        startTime: new Date(add.startTime),
        endTime: new Date(add.endTime)
      };
      delete add.objectId;
      delete add.startTime;
      delete add.endTime;


      if (id) {
        action = RestSer.lottery.put(id, add);
        text = '修改';
      } else {
        action = RestSer.lottery.add(add);
      }

      vm.loading = true;
      action.then(function() {
        AlertSer.show({
          type: 'success',
          msg: text + '抽奖成功'
        });
        vm.loading = false;
        vm.addTap();
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: text + '抽奖失败'
        });
        vm.loading = false;
      });
    };

    /**
     * 删除单条记录
     */
    vm.remove = function() {
      vm.loading = true;
      RestSer.lottery.remove(vm.add.objectId).then(function() {
        AlertSer.show({
          type: 'success',
          msg: '删除抽奖活动成功'
        });
        vm.getList();
        vm.loading = false;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: '添加抽奖活动失败'
        });
        vm.loading = false;
      });
    };

    // upload on file select or drop
    vm.upload = function(file) {
      Upload.upload({
        url: '/api/upload',
        data: {
          file: file
        }
      }).then(function(resp) {
        $log.log(resp);
        var data = resp.data;
        vm.add.image = data.url;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: '上传图片失败'
        });
      }, function() {

      });
    };

  }
})();
