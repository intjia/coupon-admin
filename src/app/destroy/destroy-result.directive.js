(function() {
  'use strict';

  angular
    .module('webProject')
    .directive('destroyResult', destroyResult);

  /** @ngInject */
  function destroyResult() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/destroy/destroy-result.html',
      scope: {
          result: '@'
      },
      controller: MotherResultController,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;

    /** @ngInject */
    function MotherResultController($scope, moment, RestSer, AlertSer) {
      var vm = this, giftId = vm.result;
          vm.moment = moment;

      vm.getList = function(page, limit){
        vm.loading = true;

        vm.result = false;
        if (vm.editor) {
          vm.editor = false;
        }

        if(page && page === vm.page){
          return;
        }

        page = parseInt(page) || 1;
        limit = limit || 30;
        vm.coupons = [];
        // 取妈妈Pa列表数据
        RestSer.coupon.find({limit: limit, skip: (page - 1)* limit, gift: giftId, 'include[0]':'user'}).then(function(coupons) {
          vm.coupons = coupons;
          vm.loading = false;
        }, function() {
          AlertSer.show({
            type: 'danger',
            msg: '加载报名数据失败,请重试'
          });
          vm.loading = false;
        });

        /**
         * 查询记录总条目数
         * @return {[type]} [description]
         */
        RestSer.coupon.count({motherPa: giftId}).then(function(count){
          var len = Math.ceil(count/limit), i, pages = [];
          for( i = 1; i < len + 1; i++){
            pages.push(i);
          }
          vm.pages = pages;
          vm.page = page;
        });

      };

      vm.getList();

    }
  }

})();
