(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('BusinessController', BusinessController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function BusinessController($firebaseRef, excelSer, $firebaseArray, $timeout, $window, $log, moment, RestSer, AlertSer, Upload, authSer) {
    var vm = this;
    vm.moment = moment;
    vm.isActive = true;

    vm.users = $firebaseArray($firebaseRef.userBusiness);
    vm.prizes = $firebaseArray($firebaseRef.prizes);
    vm.activitys = $firebaseArray($firebaseRef.activity);

    // 计算数组长度
    // var lengCount = function (refs) {
    //   refs.on('value',function (dataSnapshot) {
    //     var num = dataSnapshot.numChildren();
    //     return num;
    //   });
    // };
    // console.log(a);

    // $firebaseRef.prizes.on('value',function (dataSnapshot) {
    //                     window.a = dataSnapshot.numChildren();
    //                     // return a;
    //                   });

    // 显示添加视图
    vm.addTap = function(parent) {
      vm.editor = true;
      vm.add = {
        parent:parent,
        password: '123456'
      };
    };

    vm.download = function(){
      excelSer.outPut([['姓名', '年龄'], ['张学', 18]]);
    };

    vm.editorTap = function(item){
        vm.copyCat = angular.copy(item);
    };

    vm.editorSave = function(item){
      for(var i in item){
        if(i.indexOf('$') === -1 && vm.copyCat[i] !== item[i]){
          item[i] = vm.copyCat[i];
        }
      }
      vm.users.$save(item);
          vm.copyCat = null;
      // .then(function(data){
      //     vm.copyCat = null;
      // }).catch(function(err){
      //   AlertSer.show({
      //     type: 'danger',
      //     msg: '保存失败'
      //   });
      // });
    };


    // 查看商家处兑换的优惠券
    // var lookCoupon = function () {
    //   vm.bId = '0e1db449-293a-4a19-b5fb-076a8bf9d324';
    //   vm.activitys.$loaded().then(function (activitys) {
    //     var loopActive = activitys;
    //     vm.prizes.$loaded().then(function (prizes) {
    //       var loopPrize = prizes;
    //       // var loopActive = vm.activitys;
    //       // 获取某商家处兑换的红包
    //       for (var i = 0; i < loopPrize.length; i++) {
    //         if ( loopPrize[i].exchanged === vm.bId) {
    //           // var rPrizes = {};
    //           // vm.rPrizes = loopPrize[i];
    //           console.log('红包列表循环成功');
    //           // console.log(rPrizes);
    //           var loopActiveId = vm.prizes[i].activity;
    //           var loopPrizesKey = vm.prizes[i].prizeKey;
    //           // 获取红包所属活动
    //           for (var ii = 0; ii < loopActive.length; ii++) {
    //             console.log('获取活动循环成功');
    //             if (loopActive[ii].$id === loopActiveId) {
    //               console.log('获取活动循环成功');
    //               loopPrize.activeName = loopActive[ii].name;
    //               // 获取红包的各项属性
    //               for (var iii = 0; iii < loopActive.prize.length; iii++) {
    //                 if (loopActive.prize[iii].$id === loopPrizesKey) {
    //                   vm.prizes.prizeName = loopActive.prizes[iii].name;
    //                 }
    //               }
    //             }
    //           }
    //         }
    //       }
    //     });
    //   });
    // };
    // lookCoupon();

    /**
     * 添加商家
     */
    vm.save = function() {

      vm.loading = true;
      authSer.createUser(angular.copy(vm.add)).then(function(){
        AlertSer.show({
          type: 'success',
          msg: '添加商家成功'
        });
        vm.add = {};
        vm.editor = false;
        vm.loading = false;

      }, function(err){

        var say = '';
        switch (err.code) {
          case 'EMAIL_TAKEN':
            say = '该邮箱已存在';
            break;
          case 'INVALID_EMAIL':
            say = '邮箱格式不正确';
            break;
          default:
            say = '添加失败';
        }
        AlertSer.show({
          type: 'danger',
          msg: say
        });
        vm.loading = false;

      });

    };

  }
})();
