(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($filter, $scope, $firebaseRef, $firebaseArray, $timeout, webDevTec, toastr) {
    var vm = this;
    var statistic = [];
    vm.statistic = $firebaseArray($firebaseRef.activity);

    vm.result = {};

    $scope.$watch('activity', function(){
      vm.count();
    });

    /**
     * 红包数据统计
     */
     vm.count = function(){
       var activity = vm.statistic.$indexFor($scope.activity);
          activity = vm.statistic[activity];
       var tongji = {
         total: 0,
         remain: 0,
         destroy: 0,
         none: 0, //未兑换
       }, i;
       if(activity){
         if(angular.isObject(activity.prize)){

           for(i in activity.prize){

             // 红包总个数
             if(angular.isNumber(activity.prize[i].total)){
               tongji.total += activity.prize[i].total;
             }

             //  已兑换个数
             if(angular.isNumber(activity.prize[i].destroy)){
               tongji.destroy += activity.prize[i].destroy;
             }

             //  剩余红包个数
             if(angular.isNumber(activity.prize[i].remain)){
               tongji.remain += activity.prize[i].remain;
             }


           }

         }

       }
       //  红包统计数据
       vm.hbdata = [tongji.total - tongji.remain, tongji.remain];
      //  兑换统计数据
      vm.dhdata = [tongji.destroy, tongji.total - tongji.remain - tongji.destroy];
     };

     /**
      * 监听数据变化
      * 初始化后默认显示第一条数据
      * 数据变化后重新渲染结果
      */
     vm.statistic.$watch(function(event) {
      //  如果没有默认选中活动，则默认选中第一条活动
       if(!$scope.activity){

         if(vm.statistic.length){
           $scope.activity = vm.statistic[0].$id;
         }

       }
       vm.count();
     });

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1458444388702;
    vm.showToastr = showToastr;

    // 统计图模拟数据
    vm.hblabels = ['已发红包', '剩余红包'];
    vm.dhlabels = ['已兑换', '未兑换'];
    vm.hbdata = [];
    vm.dhdata = [];
    vm.legend = true;
    vm.hbcolours = ['#00BCD4', '#2196F3', '#FDB45C'];
    vm.dhcolours = ['#FFC107', '#FF5722', '#949FB1', '#4D5360'];

    activate();

    function activate() {
      getWebDevTec();
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }
  }
})();
