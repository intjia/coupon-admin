(function() {
  'use strict';

  angular
    .module('webProject')
    .directive('motherResult', motherResult);

  /** @ngInject */
  function motherResult() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/mother/mother-result.html',
      scope: {
          result: '@'
      },
      controller: MotherResultController,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;

    /** @ngInject */
    function MotherResultController($scope, moment, RestSer, AlertSer) {
      var vm = this, motherId = vm.result;

      RestSer.mother.get(motherId).then(function(result){
        vm.attr = result.attr;
      }, function(){
        AlertSer.show({
          type: 'danger',
          msg: '加载报名数据失败,请重试'
        });
      });


      vm.getList = function(page, limit){

        vm.result = false;
        if (vm.editor) {
          vm.editor = false;
        }

        if(page && page === vm.page){
          return;
        }

        page = parseInt(page) || 1;
        limit = limit || 30;
        vm.mothers = [];
        // 取妈妈Pa列表数据
        RestSer.mpsignin.find({limit: limit, skip: (page - 1)* limit, motherPa: motherId}).then(function(mothers) {
          vm.mothers = mothers;
        }, function() {
          AlertSer.show({
            type: 'danger',
            msg: '加载报名数据失败,请重试'
          });
        });

        /**
         * 查询记录总条目数
         * @return {[type]} [description]
         */
        RestSer.mpsignin.count({motherPa: motherId}).then(function(count){
          var len = Math.ceil(count/limit), i, pages = [];
          for( i = 1; i < len + 1; i++){
            pages.push(i);
          }
          vm.pages = pages;
          vm.page = page;
        });

      };

      vm.getList();

    }
  }

})();
