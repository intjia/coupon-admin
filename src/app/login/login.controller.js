(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($timeout, $log, $state, authSer, AlertSer) {

    var vm = this;

    vm.login = function(){

      vm.loading = true;
      authSer.login(vm.user.username, vm.user.password).then(function(){
        $state.go('home');
        vm.loading = false;
      }, function(err){
        var code = err && err.code || false;
        if(code === 210){
          code = '用户名或密码错误';
        }else if(code === 211){
          code = '该用户不存在';
        }else{
          code = '登录失败';
        }
        AlertSer.show({
          type: 'danger',
          msg: code
        });
        $log.error('登录失败', err);
        vm.loading = false;
      });

    };


  }
})();
