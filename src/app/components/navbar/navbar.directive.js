(function() {
  'use strict';

  angular
    .module('webProject')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };
    return directive;

    /** @ngInject */
    function NavbarController(moment, $window, $state) {
      var vm = this,
          $ = $window.$;

      vm.stateName = $state.name;

      vm.list = [
        {
          name: '概况',
          state: 'home'
        },
        // {
        //   name: '抽奖',
        //   state: 'lottery'
        // },
        {
          name: '红包管理',
          state: 'prize'
        },
        {
          name: '商户管理',
          state: 'business'
        },
        {
          name: '兑换结果',
          state: 'exchange'
        }
        // },
        // {
        //   name: '投票活动',
        //   state: 'vote'
        // },
        // {
        //   name: '妈妈 PA',
        //   state: 'mother'
        // }
      ];

      vm.go = function(item){
        $state.go(item.state);
        vm.stateName = item.name;
      };

    }
  }
})();
