(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('LotteryResultController', LotteryResultController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function LotteryResultController($timeout, $state, $log, moment, RestSer, AlertSer) {
    var vm = this,
      id = $state.params.id;

    vm.moment = moment;
    vm.resultLog = ['firstLog', 'secondLog', 'thirdLog', 'otherLog'];
    vm.resultType = ['firstPrize', 'secondPrize', 'thirdPrize', 'otherPrize'];


    RestSer.lotterylog.count({lottery: id}).then(function(count){
      vm.length = count;
    });


    vm.get = function() {
      RestSer.lottery.get(id).then(function(result) {
        vm.prize = result;
      }, function() {
        AlertSer.show({
          type: 'danger',
          msg: '抽奖详细加载失败,请重试'
        });
      });
    };

    vm.get();

  }
})();
