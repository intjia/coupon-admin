(function() {
  'use strict';

  angular
    .module('webProject', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'restangular', 'ui.router', 'ui.bootstrap', 'toastr', 'ngFileUpload', 'moment-picker', 'firebase','chart.js']);

})();
