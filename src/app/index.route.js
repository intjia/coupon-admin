(function() {
  'use strict';

  angular
    .module('webProject')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

      // // 抽奖
      // .state('destroy', {
      //   url: '/destroy',
      //   templateUrl: 'app/destroy/destroy.html',
      //   controller: 'DestroyController',
      //   controllerAs: 'destroy'
      // })


      // // 投票排行
      // .state('vote', {
      //   url: '/vote',
      //   templateUrl: 'app/signin/signin.html',
      //   controller: 'SigninController',
      //   controllerAs: 'signin'
      // })

      // 登录
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })

      // 商家管理
      .state('business', {
        url: '/business',
        templateUrl: 'app/business/business.html',
        controller: 'BusinessController',
        controllerAs: 'business'
      })


      // 红包
      .state('prize', {
        url: '/prize',
        templateUrl: 'app/prize/prize.html',
        controller: 'PrizeController',
        controllerAs: 'prize'
      })



      // 兑换结果
      .state('exchange', {
        url: '/exchange',
        templateUrl: 'app/exchange/exchange.html',
        controller: 'ExchangeController',
        controllerAs: 'ex'
      })

      // 抽奖结果
      .state('lottery-result', {
        url: '/lottery/:id',
        templateUrl: 'app/lottery/result.html',
        controller: 'LotteryResultController',
        controllerAs: 'rs'
      });


    $urlRouterProvider.otherwise('/');
  }

})();
