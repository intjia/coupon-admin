(function() {
  'use strict';

  angular
    .module('webProject')
    .run(stateChange)
    .run(httpConfig)
    // .run(refTest)
    .run(runBlock);

  /**
   * 测试angularfire的RefProvider
   */
  // function refTest($firebaseRef, $rootScope, $firebaseArray) {
  //   $rootScope.firetest = $firebaseArray($firebaseRef.default);
  //   $rootScope.firetest.$add(123);
  //   $rootScope.firetest.$add({'name': 'Melse', 'todo': 'testWithUserAuth'});
  // }

  function httpConfig($http, $cookies) {
    //$http({withCredentials: true});
    //

    // console.log($cookies);
    $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
  }

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

  /**
   * stateChange 地址栏状态变化
   */
  function stateChange($rootScope, $log, $location, $window, authSer, AlertSer) {

    // 监听 state change
    $rootScope.$on('$stateChangeStart', function(event, next) {
      var isLogin = authSer.isLogin();
      var nextName = next.name;
      // 没有登录去登录
      if (!isLogin) {
        $location.path('/login');
      } else {
        // 已经登录，禁止去往登录页面
        if (nextName === 'login') {
          $location.path('/');
        }
      }

    });

    $rootScope.$on('sessionOut', function() {
      $location.path('/login');
      authSer.logout();
      AlertSer.show({
        type: 'danger',
        msg: '登录状态已过期，请重新登录'
      });
    });

    $rootScope.logout = authSer.logout;
  }

})();
