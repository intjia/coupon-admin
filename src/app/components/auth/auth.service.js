(function() {
  'use strict';

  angular
    .module('webProject')
    .factory('authStatus', authStatus)
    .service('authSer', authSer);

  /**
   * HTTP 错误码拦截
   */
  function authStatus($q, $rootScope) {
    return {
      responseError: function(response) {
          var status = response.status;
          // 没有登录
          if(status === 401){
            // 广播 session 失效
            $rootScope.$broadcast('sessionOut');
          }
        return $q.reject(response);
      }
    };
  }

  /** @ngInject */
  function authSer($q, $window, $rootScope, $cookieStore, $state, Restangular, host, $firebaseRef) {
    $rootScope.user = $cookieStore.get('user') || false;

    var auth = {
      login: login,
      logout: logout,
      isLogin: isLogin,
      createUser: createUser
    };
    return auth;

    /**
     * 登录
     */
    function login(username, password) {

      var deferred = $q.defer();
      // var loginData = {
      //   'username': username,
      //   'password': password
      // };


      // 登录
      $firebaseRef.default.authWithPassword({
          'email': username,
          'password': password
      }, function(err, authData){
        if(err){
          err = JSON.parse(JSON.stringify(err));
          if(err.code === 'INVALID_EMAIL'){
            err.code = 211;
          }else if(error.code === 'INVALID_PASSWORD'){
            err.code = 210;
          }
          error(err);
        }else {
          $firebaseRef.userAdmin.child(authData.uid).once('value', function(data){
            if(data.val() !== null){
              success(authData);
            }else{
              error(err);
            }
          }, function(err){
            error(err);

          });
        }
      });


      function success(result) {
        // 设置cookie 过期时间为30天
        var exp = new $window.Date($window.Date.now() + (3600000 * 24 * 30) - 5000);
        $cookieStore.put('user', result, {
          expires: exp
        });
        $rootScope.user = result;
        // 计算显示视图
        deferred.resolve(result);
      }

      function error(result) {
        deferred.reject(result);
        $firebaseRef.default.unauth();
      }

      return deferred.promise;
    }

    /**
     * 退出
     */
    function logout() {
      $firebaseRef.default.unauth();
      $cookieStore.remove('user');
      $rootScope.user = null;
      $state.go('login');
    }

    /**
     * 是否登录
     */
    function isLogin() {
      var isLog = $firebaseRef.default.getAuth();
      // var isLog = $rootScope.user || $cookieStore.get('user') || false;
      if (isLog && !$rootScope.user) {
        $rootScope.user = isLog;
      }
      return isLog;
    }


    /**
     * 创建商家
     */
    function createUser(user){
      var deferred = $q.defer();
      $firebaseRef.default.createUser({
        email: user.email,
        password: user.password
      }, function(err, userData){
        if (err) {
          deferred.reject(err);
        } else {
          var info = {
            phone: user.phone,
            address: user.address,
            name: user.name,
            createdAt: Firebase.ServerValue.TIMESTAMP
          };

          if(user.parent){
            info.parent = user.parent;
          }

          // 创建成功
          $firebaseRef.userBusiness.child(userData.uid).set(info);
          deferred.resolve(userData);
        }
      });

      return deferred.promise;

    }

  }

})();
