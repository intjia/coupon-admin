(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('SigninController', SigninController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function SigninController($timeout, $log, RestSer) {
    var vm = this;
    vm.alerts = [];
    vm.limit = 20;

    vm.getFeature = function(){
      RestSer.feature.find().then(function(features){
        vm.features = features;
      });
    };
    vm.getFeature();
    // 加载活动

    // 切换结果
    vm.showList = function(item){
      vm.showFeature = item;
      vm.getList(true);
      vm.getCount();
    };

    vm.getList = function(page, limit) {
      if (vm.editor) {
        vm.editor = false;
      }

      if(page && page === vm.page){
        return;
      }

      if(page === true){
        page = 1;
      }

      page = parseInt(page) || 1;
      vm.signins = [];
      vm.page = page;
      // 取抽奖列表数据
      RestSer.signin.find({limit: vm.limit, skip: (page - 1)* limit, feature: vm.showFeature.objectId, descending: 'votesNumber'}).then(function(signins) {
        vm.signins = signins;
      }, function() {

      });

    };
    /**
     * 查询记录总条目数
     * @return {[type]} [description]
     */
    vm.getCount = function(){
      RestSer.signin.count({feature: vm.showFeature.objectId}).then(function(count){
        var len = Math.ceil(count/vm.limit), i, pages = [];
        for( i = 1; i < len + 1; i++){
          pages.push(i);
        }
        vm.pages = pages;
      });

    };


  }
})();
