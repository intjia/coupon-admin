(function() {
  'use strict';

  angular
      .module('webProject')
      .service('AlertSer', AlertSer);

  /** @ngInject */
  function AlertSer($timeout) {

    function Alert(option){

      if(!angular.element('.alert-modules').length){
        angular.element('body').append('<div class="alert-modules"></div>');
      }

      var className = 'alert' + (new Date()).getTime();

      if(!option.type){
        option.type = 'info';
      }

      if(!option.msg){
        option.msg = '';
      }

      function html(type, msg, className){
        return  ' <div class="alert alert-'+ type +' '+ className +' alert-dismissible" role="alert"><button type="button" class="close"  data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+ msg +'</div>';
      }

      angular.element('.alert-modules').append(html(option.type, option.msg, className));
      angular.element('.' + className + ' .close').click(function(){
        angular.element('.' + className).remove();
      });

      $timeout(function(){
        angular.element('.' + className).remove();
      }, 5000);

    }

    return {
      show: Alert
    };
  }

})();
