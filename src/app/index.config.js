(function() {
  'use strict';

  angular
    .module('webProject')
    .config(httpConfig)
    .config(momentPickerConfig)
    .config(config)
    .config(fireRef);

  /**
   * 配置Firebase引用
   */
  function fireRef(FirebaseUrl, FirebaseUser, $firebaseRefProvider) {
    $firebaseRefProvider.registerUrl({
      default: FirebaseUrl, //  'https://yejingji.firebaseio.com/midNight/'
      prizes: FirebaseUrl + 'prizes', //  'https://yejingji.firebaseio.com/midNight/prizes' 奖品Ref
      activity: FirebaseUrl + 'activity', //  'https://yejingji.firebaseio.com/midNight/prizes' 奖品Ref
      exchange: FirebaseUrl + 'exchange', //  'https://yejingji.firebaseio.com/midNight/exchange' 奖品兑换Ref
      user: FirebaseUser, // 用户
      userInfo: FirebaseUser + 'users', // 用户详细信息
      userBusiness: FirebaseUser + 'business', // 商户权限
      userAdmin: FirebaseUser + 'admin', // 管理权限
      visitorNum: 'https://yejingji.firebaseio.com/visitorNum/num'
    });
  }

  /** @ngInject */
  function config($logProvider, toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
  }

  /**
   * 日期选择器
   */
  function momentPickerConfig(momentPickerProvider) {
    momentPickerProvider.options({
      /* Picker properties */
      locale: 'zh-cn'
    });
  }

  /**
   * http config
   * 区分开发环境
   */
  function httpConfig($httpProvider, RestangularProvider) {
    // request 请求 发送 cookies
    $httpProvider.defaults.withCredentials = true;

    var config = {

      // http 请求超时时间
      httpTimeout: 20000,

      // Api 的请求地址
      apiHost: 'http://localhost:3000',

      // 运行环境的 host
      host: 'http://localhost:9000'
    };
    // 判断是否为开发环境
    var host = window.location.host;
    if (('http://' + host) !== config.host) {
      config.apiHost = 'http://' + host;
    }


    var httpTimeout = config.httpTimeout;
    var apiHost = config.apiHost;
    //   设置 Restangular 默认值
    RestangularProvider.setRestangularFields({
      id: 'objectId'
    });

    // HTTP 错误码拦截
    $httpProvider.interceptors.push([
      '$injector',
      function($injector) {
        return $injector.get('authStatus');
      }
    ]);

    $httpProvider.interceptors.push([function() {
      return {
        request: function(config) {
          config.timeout = httpTimeout;

          // 当 url 中没有 http 或者 https 的时候，自动拼接默认的 apiHost
          if (!/^[http|https]/.test(config.url) && !/\.html$/.test(config.url) && !/\.svg/.test(config.url)) {
            config.url = apiHost + config.url;
          }
          return config;
        },
        response: function(response) {
          return response;
        }

      };
    }]);
  }


})();
