(function() {
  'use strict';

  angular
    .module('webProject')
    .controller('PrizeController', PrizeController);

  /** @ngInject */
  /** jsconfig AlertSer **/
  function PrizeController($firebaseRef, $firebaseArray, $timeout, $window, $log, moment, RestSer, AlertSer, Upload) {
    var vm = this;
    vm.moment = moment;
    vm.isActive = true;

    // vm.prizes = $firebaseArray($firebaseRef.prizes);
    vm.activity = $firebaseArray($firebaseRef.activity);

    /**
     * 编辑添加活动
     * @return {[type]} [description]
     */
    vm.addTap = function(item){
      vm.check = {};
      var i, stage, start = false, now = new Date();
      vm.oldEditor = false;
      if(!item){
        vm.addLoading = true;
        vm.activity.$add({name: ''}).then(function(data){
             var id = data.key();
             vm.add = vm.activity[vm.activity.$indexFor(id)];
             vm.prizeRef = $firebaseArray($firebaseRef.activity.child(id + '/prize'));
             vm.stageRef = $firebaseArray($firebaseRef.activity.child(id + '/stage'));
             vm.editor = true;
             vm.addLoading = false;
        }, function(err){
          console.log(err);
        });
      }else{
        vm.add = item;
        /**
         * 验证活动是否已经开始，如果已经开始，则禁止修改
         * 如果有一个阶段的活动已经开始，则活动已经开始，禁止修改。
         */
         if(item && item.stage){
           stage = item.stage;

           if(angular.isObject(stage)){

             for(i in stage){
              //
               if(stage[i].start && new Date(stage[i].start) < now){
                 start = true;
                 break;
               }

             }
           }

         }
         if(start){
            return alert('活动已经开始，不能修改数据');
         }
         vm.oldEditor = true;
        vm.editor = true;
        vm.prizeRef = $firebaseArray($firebaseRef.activity.child(item.$id + '/prize'));
        vm.stageRef = $firebaseArray($firebaseRef.activity.child(item.$id + '/stage'));
      }
    };

    vm.test = /(?!0000)[0-9]{4}-((0[1-9]|1[0-2])-(0[1-9]|1[0-9]|2[0-8])|(0[13-9]|1[0-2])-(29|30)|(0[13578]|1[02])-31) [0-2][0-9]:[0-5][0-9]/;
    /**
     * 删除活动
     */
    vm.removeActive = function(item){
      vm.activity.$remove(item);
    };

    /**
     * 添加奖品
     */
    vm.addPrize = function(){
      console.log(vm.add);
      vm.prizeLoading = true;

      vm.prizeRef.$add({activity: vm.add.$id}).then(function(){
        vm.prizeLoading = false;
      }, function(){
        vm.prizeLoading = false;
        AlertSer.show({
          type: 'danger',
          msg: '添加失败，请重试'
        });
      });
    };

    /**
     * 添加阶段
     */
    vm.addStage = function(){
      vm.stageRef.$add({activity: vm.add.$id});
      // if(!vm.add.stage){
      //   vm.add.stage = [];
      // }
      // vm.add.stage.push({});
    };


    /**
     * 选择奖品
     */
    vm.checkPrize = function(item, id){
      var check = vm.check[item.name + id];
      // 如果已经选中
      if(check){
        if(!item.prize){
          item.prize = {};
        }
        item.prize[id] = item.prize[id] || 0;
      }else{
        if(item.prize){
          delete item.prize[id];
        }
      }
    };


    vm.saveAll = function(){
      vm.activity.$save(vm.add);
      vm.add = null;
      vm.editor = null;
    };


    /**
     * 删除阶段
     */
      // vm.removeStage = function(stageList, index){
      //   // stageList.splice(index, 1);
      // };

//     // 显示添加视图
//     vm.addTap = function() {
//       vm.editor = true;
//       vm.cacheSave = null;
//       vm.add = {};
//     };
//
// /**
//  * 删除活动
//  */
//   vm.removeTap = function(item){
//     vm.prizes.$remove(item).then(function(){
//       console.log('deleted')
//     }, function(err){
//       console.error(err);
//     });
//   };
//
//
//
//     vm.editorTap = function(item){
//         vm.add = angular.copy(item);
//         console.log(item);
//         console.log(vm.add);
//         vm.cacheSave = item;
//         vm.editor = true;
//     };
//
//     /**
//      * 编辑活动保存
//      */
//     vm.editorSave = function(item){
//       for(var i in vm.add){
//         if(vm.add[i] !== item[i]){
//           item[i] = vm.add[i];
//         }
//       }
//       vm.prizes.$save(item);
//       vm.add = null;
//       vm.addPrize = null;
//       vm.editor = false;
//     };
//
//
//
//     /**
//      * 添加阶段
//      */
//      vm.addStage = function(){
//        if(!vm.add.stage){
//          vm.add.stage = [];
//        }
//        vm.add.stage.push({prize:[{}]});
//      };
//
//      /**
//       * 分阶段添加奖品
//       * @param {Object} item 活动对象
//       */
//      vm.addPrize = function(item){
//        if(!item.prize){
//          item.prize = [];
//        }
//        item.prize.push({});
//      };
//
//      /**
//       * 删除奖品
//       */
//      /**
//       * 删除奖品列表中的单个奖品
//       * @param  {array} prizeList 奖品列表
//       * @param  {number} index 讲评所在位置
//       */
//      vm.removePrize = function(prizeList, index){
//        prizeList.splice(index, 1);
//      };
//      /**
//       * 删除活动阶段
//       * @param  {array} stageList 阶段列表
//       * @param  {number} index   待删除阶段索引
//       */
//      vm.removeStage = function(stageList, index){
//        stageList.splice(index, 1);
//      };
//
//
//
//
//     /**
//      * 添加红包
//      * 如果是修改红包，则使用修改红包的方法
//      */
//     vm.save = function(cacheSave) {
//       if(cacheSave){
//         return vm.editorSave(cacheSave);
//       }
//       vm.add.createdAt = Firebase.ServerValue.TIMESTAMP;
//       vm.prizes.$add(angular.copy(vm.add));
//       vm.add = null;
//       vm.editor = false;
//     };
//
//     /**
//      * 添加活动
//      */
//      vm.addActivity = function(){
//        vm.activity.$add(vm.add).then(function(data){
//          var id = data.key();
//          vm.addId = id;
//          vm.add = vm.activity[vm.activity.$indexFor(id)];
//          vm.prizes.$add({activity: id});
//          console.log(data);
//        });
//        vm.addPrize = [{}];
//      };
//
//      /**
//       * 保存奖品
//       */
//
//       vm.savePrize = function(item){
//         vm.prizes.$save(item);
//       };
//
//       /**
//        * 删除奖品
//        */
//       vm.removePrize = function(item){
//         vm.prizes.$remove(item);
//       };



  }
})();
